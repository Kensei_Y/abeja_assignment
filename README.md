# Version
```
Node v4.4.0
Express 4.13.1
Mongodba 2.4.8
```

# Start Up
```
npm install .
npm start
```

## Import Mock Data
```
mongoimport --db abeja --collection states --drop --jsonArray --file ./mock/dist/mock.json
```
## Generate Mock Data(if required)
```
gulp mock
```

# Access
```
http://localhost:3000/dashboard
```

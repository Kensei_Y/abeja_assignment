var mongoose = require('mongoose');

exports.setup = function(){
  var Schema = mongoose.Schema;

  var StateSchema = new Schema({
      age : { type: Number, min: 1, required: true },
      gender : { type: String, enum: ['male', 'female'], required: true },
      feeling : { type: Number, min: 0, max: 5, required: true },
      hunger : { type: Number, min: 0, max: 5, required: true },
      sleepiness : { type: Number, min: 0, max: 5, required: true },
  }, {
    timestamps: true
  });
  mongoose.model('State', StateSchema);
};



var COLOR_SETTING = {
  TOTAL : '#3a3a3c',
  MALE  : '#d3e5f1',
  FEMALE: '#f9d2df'
}

var option = {
  chart: {
    renderTo: 'chart-container',
    type: 'column'
  },
  yAxis: {
    title: {
      text: 'Huger'
    },
    max : 5
  }
};

/*
 * calculate average
 */
var calculateAverage = function(list, key)
{
  return _.reduce(list, function(memo, data) {
      return memo + data[key];
  }, 0) / (list.length === 0 ? 1 : list.length);

}

/*
 * show time based chart
 */
var showTimeSpanChart = function(data, type)
{
  var timeRanges = _.range(0, 24);
  var timeRangeStates = {};

  // set empty data
  _.each(timeRanges, function(hour) {
    timeRangeStates[hour] = {
      total : [],
      male  : [],
      female  : [],
    };
  });

  // set states to time mapped object
  _.each(data.list, function(state) {
    if (!state)
    {
      return;
    }

    var created = new Date(state.createdAt);
    var hour = created.getHours();

    var targetHourRangeStates = timeRangeStates[hour];
    if (!targetHourRangeStates)
    {
      return;
    }

    targetHourRangeStates['total'].push(state);
    // push data to gender array
    var hourGenders = targetHourRangeStates[state.gender];
    if ($.isArray(hourGenders)) {
      hourGenders.push(state);
    }
  });

  // generate average list
  var series =  _.mapObject(timeRangeStates, function(states) {
    return {
      total : calculateAverage(states.total, type),
      male : calculateAverage(states.male, type),
      female : calculateAverage(states.female, type)
    };
  });

  // create chart, based on time
  var timeSpanChartOption = _.extend(option, {
    title: {
      text: type.toUpperCase() + '/TIME'
    },
    xAxis: {
      categories: timeRanges
    },
    yAxis: {
      title: {
        text : type.toUpperCase()
      },
      max : 5
    },
    series:[{
      name: 'Total',
      data: _.map(series, function(val) { return val.total; }),
      color : COLOR_SETTING.TOTAL
    },{
      name: 'Male',
      data: _.map(series, function(val) { return val.male; }),
      color : COLOR_SETTING.MALE
    },
    {
      name: 'Female',
      data: _.map(series, function(val) { return val.female; }),
      color : COLOR_SETTING.FEMALE
    }]
  });
  var chart = new Highcharts.Chart(timeSpanChartOption);
}

/*
 * show age based chart
 */
var showAgeSpanChart = function(data, type)
{
  var ageRanges = _.range(0, 100, 10);
  var ageRangeStates = {};

  // set empty data
  _.each(ageRanges, function(age) {
    ageRangeStates[age] = {
      total : [],
      male  : [],
      female  : [],
    };
  });

  // set states to age mapped object
  _.each(data.list, function(state) {
    if (!state)
    {
      return;
    }

    var age = Math.floor(state.age / 10) * 10;

    var targetAgeRangeStates = ageRangeStates[age];
    if (!targetAgeRangeStates)
    {
      return;
    }

    targetAgeRangeStates['total'].push(state);
    // push data to gender array
    var ageGenders = targetAgeRangeStates[state.gender];
    if ($.isArray(ageGenders)) {
      ageGenders.push(state);
    }
  });

  // generate average list
  var series =  _.mapObject(ageRangeStates, function(states) {
    return {
      total : calculateAverage(states.total, type),
      male : calculateAverage(states.male, type),
      female : calculateAverage(states.female, type)
    };
  });

  // create chart, based on age
  var ageSpanChartOption = _.extend(option, {
    title: {
      text: type.toUpperCase() + '/AGE'
    },
    xAxis: {
      categories: ageRanges
    },
    yAxis: {
      title: {
        text : type.toUpperCase()
      },
      max : 5
    },
    series:[{
      name: 'Total',
      data: _.map(series, function(val) { return val.total; }),
      color : COLOR_SETTING.TOTAL
    },{
      name: 'Male',
      data: _.map(series, function(val) { return val.male; }),
      color : COLOR_SETTING.MALE
    },
    {
      name: 'Female',
      data: _.map(series, function(val) { return val.female; }),
      color : COLOR_SETTING.FEMALE
    }]
  });
  var chart = new Highcharts.Chart(ageSpanChartOption);
}

/*
 * show chart (and overview)
 */
var showChart = function(data, yAxisType, xAxisType) {
  showOverview(data, yAxisType);

  if (xAxisType == 'time')
  {
    showTimeSpanChart(data, yAxisType);
  }
  if (xAxisType == 'age')
  {
    showAgeSpanChart(data, yAxisType);
  }
}

/*
 * setup app
 */
var setup = function(initialData, type)
{
  // dafault data
  var states = initialData;
  var filter = {};
  var dataType = 'feeling';
  var xAxisType = 'time';

  // setup events for changing data type
  var dataTypeButtons = {
    feeling    : $("#type-feeling-button"),
    hunger     : $("#type-hunger-button"),
    sleepiness : $("#type-sleepiness-button"),
  }
  dataTypeButtons['feeling'].on('click', function() {
    _.each(dataTypeButtons, function(button) { button.removeClass('active'); });
    dataTypeButtons['feeling'].addClass('active');
    dataType = 'feeling';
    showChart(states, dataType, xAxisType);
  });

  dataTypeButtons['hunger'].on('click', function() {
    _.each(dataTypeButtons, function(button) { button.removeClass('active'); });
    dataTypeButtons['hunger'].addClass('active');
    dataType = 'hunger';
    showChart(states, dataType, xAxisType);
  });

  dataTypeButtons['sleepiness'].on('click', function() {
    _.each(dataTypeButtons, function(button) { button.removeClass('active'); });
    dataTypeButtons['sleepiness'].addClass('active');
    dataType = 'sleepiness';
    showChart(states, dataType, xAxisType);
  });

  // setup events for changing xAxis type
  $("#time-span-button").on('click', function() {
    xAxisType = 'time';
    showChart(states, dataType, xAxisType);
  });
  $("#age-span-button").on('click', function() {
    xAxisType = 'age';
    showChart(states, dataType, xAxisType);
  });

  // setup events for changing time span
  $("#start-date-form").datepicker({
    onSelect: function(i,o,p) {
      var startDate = new Date(i);
      filter['start'] = startDate.getTime();
      loadStates(function(data) {
        states = data;
        showChart(states, dataType, xAxisType);
      }, filter);
    }
  });
  $("#end-date-form").datepicker({
    onSelect: function(i,o,p) {
      var endDate = new Date(i);
      filter['end'] = endDate.getTime();
      loadStates(function(data) {
        states = data;
        showChart(states, dataType, xAxisType);
      }, filter);
    }
  });

  // show default chart
  showChart(states, dataType, xAxisType);
  dataTypeButtons[dataType].addClass('active');
}

/*
 * show overview of data
 */
var showOverview = function(data, type)
{
  var states = data.list || [];
  var total = states.length;

  // total respondent count
  $('#respondent-total-number').text(total.toString());

  // average
  var average = calculateAverage(states, type);
  $('#respondent-average').text(average.toString().substring(0, 5));

  // male & female ratio
  var males = _.filter(states, function(state) {
    return state.gender == 'male';
  });

  var females = _.filter(states, function(state) {
    return state.gender == 'female';
  });

  var maleRatio = (males.length / (total === 0 ? 1 : total)) * 100;
  $('#respondent-male-ratio').text(maleRatio.toString().substring(0, 5));

  var femaleRatio = (females.length / (total === 0 ? 1 : total)) * 100;
  $('#respondent-female-ratio').text(femaleRatio.toString().substring(0, 5));

}

/*
 * load states from server
 */
var loadStates = function(callback, filter)
{
  var result = $.ajax({
    type : "get",
    url : "./list",
    dataType : "json",
    contentType: "application/json",
    data : {
      filter : filter
    },
    success : function(data){
      callback(data);
    }
  });
}

/*
 * entry point
 */
$(function() {
  loadStates(function(data) {
    setup(data);
  });
});

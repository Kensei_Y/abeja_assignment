$(function() {
  var container = $('#list-container');
  // load states and simply shows data as table
  var result = $.ajax( {
    type : "get",
    url : "./list",
    dataType : "json",
    contentType: "application/json",
    success : function(data){
      _.each(data.list, function(state) {
        var date = new Date(state.createdAt);
        var created = $('<td></td>').text(date.toLocaleString());
        var age = $('<td></td>').text(state.age);
        var gender = $('<td></td>').text(state.gender);
        var feeling = $('<td></td>').text(state.feeling);
        var hunger = $('<td></td>').text(state.hunger);
        var sleepiness = $('<td></td>').text(state.sleepiness);

        var item = $('<tr></tr>').append([created, age, gender, feeling, hunger, sleepiness]);
        container.append(item);
      });
    }
  });
});

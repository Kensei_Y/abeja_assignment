$(function() {
    var form = $('#register-form');

    var defaultSliderValue = 2.5;
    var sliderOption = {
      min : 0,
      max : 5,
      step : 0.1,
      formater : function(value) {
        // 少数第２桁以降を切り捨て
        return Math.floor(value * 10) / 10;
      }
    };

    // setup slliders
    var feelingSlider = $('#feeling-slider');
    feelingSlider.slider(sliderOption);
    feelingSlider.val(defaultSliderValue);

    var hungerSlider = $('#hunger-slider');
    hungerSlider.slider(sliderOption);
    hungerSlider.val(defaultSliderValue);

    var sleepinessSlider = $('#sleepiness-slider');
    sleepinessSlider.slider(sliderOption);
    sleepinessSlider.val(defaultSliderValue);

    // submit input data to server
    form.on('submit', function(event) {
      event.preventDefault();
      $('#submit-result > .alert').hide();

      var age = $('#age').val();
      var gender = $('input[type=radio]:checked').val();
      var feeling = feelingSlider.val();
      var hunger = hungerSlider.val();
      var sleepiness = sleepinessSlider.val();

      form.removeClass('has-error');
      var result = $.ajax( {
        type : "post",
        url : "./register",
        dataType : "json",
        data : {
          age        : age,
          gender     : gender,
          feeling    : feeling,
          hunger     : hunger,
          sleepiness : sleepiness
        },
        success : function(data){
          $('#submit-result > .alert-success').show();
          form.removeClass('has-error');
        },
        error : function(data){
          $('#submit-result > .alert-danger').show();
          form.addClass('has-error');
        }
      });
    });
});

var gulp = require('gulp');
var file = require('gulp-file');
var mock = require('./mock/setting/state.js');
var util = require('util');
var moment = require("moment");
var mongodbData = require('gulp-mongodb-data');

gulp.task('mock', function () {
  var mockData = [];

  for (var i = 0; i< 100; i++)
  {
    mockData.push(mock.create());
  }

  var str = JSON.stringify(mockData, function(key, val) {
    if (moment(val, moment.ISO_8601).isValid()) {
      var d = new Date(val);
      return {"$date": d.getTime() };
    }

    return val;
  });

  return file('mock.json', str).pipe(gulp.dest('mock/dist'));
});

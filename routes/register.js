var express = require('express');
var router = express.Router();

router.get('/', function(req, res, next) {
  res.render('register', { title: 'Register' });
});

/* GET users listing. */
router.post('/', function(req, res, next) {
    var db = req.db;
    var State = db.model('State');
    var state = new State();
    state.age = req.body.age;
    state.gender = req.body.gender;
    state.feeling = req.body.feeling;
    state.hunger = req.body.hunger;
    state.sleepiness = req.body.sleepiness;

    state.save(function(err) {
      if (err) {
        console.log(err);
        res.json(400, {"err":err});
        return;
      }

      res.json({'test' : "register success"});
    });
});

module.exports = router;

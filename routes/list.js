var express = require('express');
var router = express.Router();

/* GET users listing. */
router.get('/', function(req, res, next) {

  var db = req.db;
  var State = db.model('State');

  var filter = req.query && req.query.filter;
  var query = {};

  if (filter && filter.start)
  {
    var datePeriod = {};
    if (filter.start)
    {
      datePeriod['$gte'] = new Date(parseInt(filter.start));
    }
    if (filter.end)
    {
      datePeriod['$lte'] = new Date(parseInt(filter.end));
    }
    query['createdAt'] = datePeriod;
  }

  State.find(query, function(err, docs) {
    res.json({list : docs});
  });
});

module.exports = router;


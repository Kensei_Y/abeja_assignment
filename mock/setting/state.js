var MOCK_START_DATE = new Date(2015, 3, 1);

var MOCK_END_DATE = new Date(2016, 2, 1);

function getRandomArbitary(min, max) {
  return Math.random() * (max - min) + min;
}

function getRandomInt(min, max) {
  return Math.floor( Math.random() * (max - min + 1) ) + min;
}

function getRandomDate(start, end) {
  var startTimestamp = start.getTime();
  var endTimestamp = end.getTime();
  var randomTimestamp = getRandomInt(startTimestamp, endTimestamp)
  return new Date(randomTimestamp);
}

var create = function() {
  return {
    age        : getRandomInt(0, 100),
    gender     : (Math.random() < 0.5) ? 'male' : 'female' ,
    feeling    : getRandomArbitary(0, 5),
    hunger     : getRandomArbitary(0, 5),
    sleepiness : getRandomArbitary(0, 5),
    createdAt  : getRandomDate(MOCK_START_DATE, MOCK_END_DATE)
  };
}

module.exports = {
  create : create
}
